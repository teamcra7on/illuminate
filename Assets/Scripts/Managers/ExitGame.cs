﻿using UnityEngine;
using System.Collections;

public class ExitGame : MonoBehaviour {

	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.Escape)) {
			Time.timeScale = 1.0f;
			Application.LoadLevel (0);
		}
	}
}
