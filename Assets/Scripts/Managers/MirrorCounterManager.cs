﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MirrorCounterManager : MonoBehaviour {

	public GameObject[] Mirrors;
	public GameObject[] MirrorCount;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {

		MirrorCount = GameObject.FindGameObjectsWithTag ("Mirror");

		// Set the color based on the mirror count
		// Rogi Note: variable = condition ? first_expression : second_expression; (a more concise version of if-else in one line situations)

		Mirrors [0].gameObject.GetComponent<Image> ().color = MirrorCount.Length >= 1 ? new Color (1f, 1f, 1f, 1f) : new Color (1f, 1f, 1f, 0.5f);

		Mirrors [1].gameObject.GetComponent<Image> ().color = MirrorCount.Length >= 2 ? new Color (1f, 1f, 1f, 1f) : new Color (1f, 1f, 1f, 0.5f);

		Mirrors [2].gameObject.GetComponent<Image> ().color = MirrorCount.Length >= 3 ? new Color (1f, 1f, 1f, 1f) : new Color (1f, 1f, 1f, 0.5f);

		Mirrors [3].gameObject.GetComponent<Image> ().color = MirrorCount.Length >= 4 ? new Color (1f, 1f, 1f, 1f) : new Color (1f, 1f, 1f, 0.5f);

		Mirrors [4].gameObject.GetComponent<Image> ().color = MirrorCount.Length >= 5 ? new Color (1f, 1f, 1f, 1f) : new Color (1f, 1f, 1f, 0.5f);
	
	}
}
