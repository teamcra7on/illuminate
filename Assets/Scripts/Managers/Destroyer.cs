﻿using UnityEngine;
using System.Collections;

public class Destroyer : MonoBehaviour {

	public bool Effect;
	
	void OnTriggerEnter2D (Collider2D other) {
		Destroy (other.gameObject);
	}
}
