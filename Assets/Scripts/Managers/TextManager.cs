﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextManager : MonoBehaviour {

	public Animator DialogueBox;
	public bool CanWrite;

	// Use this for initialization
	void Start () {
		CanWrite = true;
	}
		
	public void TypeText(Text textElement, string TextToBeWritten, float time ) {
		float TypeDelay = time / TextToBeWritten.Length; // How fast the Auto Type types
		textElement.StartCoroutine (SetText(textElement, TextToBeWritten, TypeDelay));
	}

	IEnumerator SetText(Text textElement, string TextToBeWritten, float TypeDelay) {

		DialogueBox.SetBool ("IsHidden", false);
		CanWrite = false;

		Time.timeScale = 0f;

		for(int i = 0; i < TextToBeWritten.Length; i++){
			textElement.text += TextToBeWritten[i];
			
			yield return StartCoroutine (CoroutineUtil.WaitforRealSeconds(TypeDelay));
		}

		yield return StartCoroutine (CoroutineUtil.WaitforRealSeconds(3f));

		GetComponent<Text> ().text = "";
		
		DialogueBox.SetBool ("IsHidden", true);
		CanWrite = true;
		Time.timeScale = 1f;
		yield return null;
	}
}
