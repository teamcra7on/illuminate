﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DialogueWriter : MonoBehaviour {

	public GameObject dialogue;
	public GameObject dialogue2;
	public GameObject dialogue3;
	TextManager DialogueSource;
	TextManager DialogueSource2; //For the Game Scene Illuminator Description
	TextManager DialogueSource3; //For the Game Scene Mirror Buster Description

	// <summary>
	// Variables for Dialogue Triggers
	// </summary>

	public static bool FirstTrigger; // Start of First Stage..
	public static bool SecondTrigger; // After First Wave...
	public static bool ThirdTrigger; // After First Wave...
	public static bool FourthTrigger; // After First Wave...
	public static bool FifthTrigger; // After First Wave...

	//(To prevent the if statements from triggerring a couple of times)

	bool FirstTriggerNotExecuted; // Check if the First Trigger is triggered or not 
	bool SecondTriggerNotExecuted; // Check if the Second Trigger is triggered or not
	bool ThirdTriggerNotExecuted; // Check if the Third Trigger is triggered or not
	bool FourthTriggerNotExecuted; // Check if the Third Trigger is triggered or not
	bool FifthTriggerNotExecuted; // Check if the Third Trigger is triggered or not

	// Use this for initialization
	void Start () {
		DialogueSource = dialogue.GetComponent<TextManager> ();
		FirstTrigger = false;
		SecondTrigger = false;
		ThirdTrigger = false;
		FourthTrigger = false;
		FifthTrigger = false;
		FirstTriggerNotExecuted = true;
		SecondTriggerNotExecuted = true;
		ThirdTriggerNotExecuted = true;
		FourthTriggerNotExecuted = true;
		FifthTriggerNotExecuted = true;
	}
	
	// Update is called once per frame
	void Update () {

		if(Time.realtimeSinceStartup > 2f) {
			FirstTrigger = true;
		}

		if(FirstTriggerNotExecuted && DialogueSource.CanWrite) {
			if(FirstTrigger == true) {
				DialogueSource.TypeText(dialogue.GetComponent<Text>(), "Brianna \n ...Placeholder Dialogue... \n Press the [Left Mouse Button] to fire a laser. \n Press the [Right Mouse Button] to fire mirrors that can reflect certain projectiles. \n Move the Mouse Scroll to set your next mirror's angle.",2f);
				FirstTriggerNotExecuted = false;
			}
		}

		if(SecondTriggerNotExecuted && DialogueSource.CanWrite) {
			if(SecondTrigger == true) {

				DialogueSource.TypeText(dialogue.GetComponent<Text>(), "Brianna \n \n ...It seems I can only place 5 mirrors at max \n More than that, I will not have sufficient energy to maintain the \n first mirror.",2f);
				SecondTriggerNotExecuted = false;
			}
		}

		if(ThirdTriggerNotExecuted && DialogueSource.CanWrite) {
			if(ThirdTrigger == true) {
				
				DialogueSource.TypeText(dialogue.GetComponent<Text>(), "Brianna \n \n ...In case I misplace a Mirror, I should always take note that I can send them \n back by pointing the cursor on them and pressing the [Mouse Scroll Button].",2f);
				ThirdTriggerNotExecuted = false;
			}
		}

		if(FourthTriggerNotExecuted && DialogueSource.CanWrite) {
			if(FourthTrigger == true) {
				
				DialogueSource.TypeText(dialogue.GetComponent<Text>(), "Brianna \n \n ...Impossible! The Illuminator can't pass through its shield?!",2f);
				FourthTriggerNotExecuted = false;
			}
		}

		if(FifthTriggerNotExecuted && DialogueSource.CanWrite) {
			if(FifthTrigger == true) {
				
				DialogueSource.TypeText(dialogue.GetComponent<Text>(), "Brianna \n \n ...I did it!",2f);
				FifthTriggerNotExecuted = false;
			}
		}

	}
}
