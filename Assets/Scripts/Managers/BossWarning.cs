﻿using UnityEngine;
using System.Collections;

public class BossWarning : MonoBehaviour {
	
	public Animator WarningVisual1;
	public Animator WarningVisual2;
	public Animator WarningVisual3;	
	public Animator WarningVisual4;
	float timer;
	// Use this for initialization
	void Start () {
		timer = 7f;
		StartCoroutine (BossAlert());
	}

	void OnDisable () {
		SendMessageUpwards ("RemoveFromPool", transform);
	}

	IEnumerator BossAlert() {

		//Activate Masks
		WarningVisual1.SetTrigger("IsPlayed");

		yield return new WaitForSeconds (0.5f);

		//Activate Warning Animations
		WarningVisual2.SetTrigger("IsPlayed");
		WarningVisual3.SetTrigger("IsPlayed");
		WarningVisual4.SetTrigger("IsPlayed");

		yield return null;
	}
	
	// Update is called once per frame
	void Update () {

		if (timer <= 0f) {
			gameObject.SetActive(false);
		}

		timer = timer - 1 * Time.deltaTime;
	}
}
