﻿using UnityEngine;
using System.Collections;

//------------------------------------------------------------------------------
// class definition
//------------------------------------------------------------------------------
public class MainController : MonoBehaviour
{
	static MainController mainController;

	string currentSceneName;
	string nextSceneName;
	AsyncOperation resourceUnloadTask;
	AsyncOperation sceneLoadTask;
	enum SceneState { Reset, Preload, Load, Unload, Postload, Ready, Run, Count };
	SceneState sceneState;
	private delegate void UpdateDelegate();
	UpdateDelegate[] updateDelegates;

	public Animator LoadBG;
	public Animator LoadAsset1;
	public Animator LoadAsset2;

	//--------------------------------------------------------------------------
	// public static methods
	//--------------------------------------------------------------------------
	public static void SwitchScene(string nextSceneName) {
		if(mainController != null) {
			if( mainController.currentSceneName != nextSceneName ) {
				mainController.nextSceneName = nextSceneName;
			}
		}
	}

	//--------------------------------------------------------------------------
	// protected mono methods
	//--------------------------------------------------------------------------
	protected void Awake() {
		//Let's keep this alive between scene changes
		Object.DontDestroyOnLoad(gameObject);

		//Setup the singleton instance
		mainController = this;

		//Setup the array of updateDelegates
		updateDelegates = new UpdateDelegate[(int)SceneState.Count];

		//Set each updateDelegate
		updateDelegates[(int)SceneState.Reset] = UpdateSceneReset;
		updateDelegates[(int)SceneState.Preload] = UpdateScenePreload;
		updateDelegates[(int)SceneState.Load] = UpdateSceneLoad;
		updateDelegates[(int)SceneState.Unload] = UpdateSceneUnload;
		updateDelegates[(int)SceneState.Postload] = UpdateScenePostload;
		updateDelegates[(int)SceneState.Ready] = UpdateSceneReady;
		updateDelegates[(int)SceneState.Run] = UpdateSceneRun;

		nextSceneName = "MainMenu";
		sceneState = SceneState.Reset;
	}
	
	protected void OnDestroy() {
		//Clean up all the updateDelegates
		if(updateDelegates != null) {
			for(int i = 0; i < (int)SceneState.Count; i++) {
				updateDelegates[i] = null;
			}
			updateDelegates = null;
		}

		//Clean up the singleton instance
		mainController = null;
	}
	
	protected void OnDisable()
	{
	}
	
	protected void OnEnable()
	{
	}
	
	protected void Start()
	{
	}
	
	protected void Update() {
		if(updateDelegates[(int)sceneState] != null) {
			updateDelegates[(int)sceneState]();
		}
	}

	//--------------------------------------------------------------------------
	// private methods
	//--------------------------------------------------------------------------
	// attach the new scene controller to start cascade of loading
	void UpdateSceneReset() {
		// run a gc pass
		System.GC.Collect();
		sceneState = SceneState.Preload;
	}

	// handle anything that needs to happen before loading
	void UpdateScenePreload() {
		sceneLoadTask = Application.LoadLevelAsync(nextSceneName);
		sceneState = SceneState.Load;

	}
	
	// show the loading screen until it's loaded
	void UpdateSceneLoad() {
	

		// done loading?
		if (sceneLoadTask.isDone) {
			
			LoadAsset1.SetBool ("IsHidden", true);
			LoadAsset2.SetBool ("IsHidden", true);
			LoadBG.SetBool ("IsHidden", true);
			sceneState = SceneState.Unload;
		} else {
			// update scene loading progress
			LoadBG.SetBool ("IsHidden", false);
			LoadAsset1.SetBool ("IsHidden", false);
			LoadAsset2.SetBool ("IsHidden", false);
		}
	}

	// clean up unused resources by unloading them
	void UpdateSceneUnload() {
		// cleaning up resources yet?
		if(resourceUnloadTask == null) {
			resourceUnloadTask = Resources.UnloadUnusedAssets();
		}
		else {
			// done cleaning up?
			if (resourceUnloadTask.isDone) {
				resourceUnloadTask = null;
				sceneState = SceneState.Postload;
			}
		}
	}

	// handle anything that needs to happen immediately after loading
	void UpdateScenePostload() {
		currentSceneName = nextSceneName;
		sceneState = SceneState.Ready;
	}


	// handle anything that needs to happen immediately before running
	void UpdateSceneReady() {
		// run a gc pass
		// if you have assets loaded in the scene that are
		// currently unused currently but may be used later
		// DON'T do this here
		System.GC.Collect();
		sceneState = SceneState.Run;
	}
	
	// wait for scene change
	void UpdateSceneRun() {
		if(currentSceneName != nextSceneName) {
			sceneState = SceneState.Reset;
		}
	}
}
