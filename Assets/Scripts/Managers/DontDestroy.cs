﻿using UnityEngine;
using System.Collections;

public class DontDestroy : MonoBehaviour {

	// Use this for initialization
	void Awake () {
		//Let's keep this alive between scene changes
		Object.DontDestroyOnLoad(gameObject);
	}
}
