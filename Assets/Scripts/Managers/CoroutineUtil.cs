﻿using UnityEngine;
using System.Collections;

public static class CoroutineUtil {

	//Alternative for WaitforSeconds if TimeScale = 0
	public static IEnumerator WaitforRealSeconds(float time) {
		float start = Time.realtimeSinceStartup;
		while (Time.realtimeSinceStartup < start + time) {
		yield return null;
		}
	}

}
