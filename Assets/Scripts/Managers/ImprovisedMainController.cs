﻿using UnityEngine;
using System.Collections;

public class ImprovisedMainController : MonoBehaviour {

	public static int CurrentScene;
	public static int NextScene;

	// Use this for initialization
	void Awake () {

		LoadScene ();
	}
	
	// Update is called once per frame
	void Update () {
		NextScene = CurrentScene + 1;
	}

	public void LoadScene() {
		StartCoroutine (LoadNextScene ());
	}
		IEnumerator LoadNextScene () {

		yield return new WaitForSeconds(2.0f);

		Application.LoadLevel (NextScene);

		yield return new WaitForSeconds(2.0f);
	}
}
