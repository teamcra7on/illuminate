﻿using UnityEngine;
using System.Collections;

public class Disabler : MonoBehaviour {

	void OnTriggerEnter2D (Collider2D other) {
		StartCoroutine (DisableObject (other));

		if (other.GetComponent<EnemyScript> () != null) {
			other.GetComponent<EnemyScript> ().DetachChildren ();
		}
	}

	IEnumerator DisableObject (Collider2D otherObject) {
		yield return new WaitForSeconds (0.5f);
		otherObject.gameObject.SetActive (false);
	}
}
