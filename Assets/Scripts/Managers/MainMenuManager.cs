﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainMenuManager : MonoBehaviour {

	public Transform Exit;
	public Transform Setting;
	public Transform Starting;

	public Animator Cover;
	public Animator StartButton;
	public Animator SettingsButton;
	public Animator ExitButton;
	public Animator PressEffect;
	public Animator PressEffect2;
	public Animator PressEffect3;
	public Animator Flash;
	public Animator Flash2;
	public Animator Flash3;
	public Animator StartFlash;
	public Animator StartInfo;
	public Animator SettingFlash;
	public Animator SettingInfo;
	public Animator ExitFlash;
	public Animator ExitInfo;

	public Animator MaskCanvas;

	public MainController MainManager;

	/// <summary>
	/// This tells what will happen if any of the 3 button is pointed at 
	/// </summary>

	public void StartisPointed() {
		StartInfo.SetBool ("IsHidden", false);
	}

	public void StartisnotPointed() {
		StartInfo.SetBool ("IsHidden", true);
	}

	public void SettingisPointed() {
		SettingInfo.SetBool ("IsHidden", false);
	}
	
	public void SettingisnotPointed() {
		SettingInfo.SetBool ("IsHidden", true);
	}

	public void ExitisPointed() {
		ExitInfo.SetBool ("IsHidden", false);
	}
	
	public void ExitisnotPointed() {
		ExitInfo.SetBool ("IsHidden", true);
	}

/// <summary>
/// If the First Button is Clicked 
/// </summary>

	public void MenuCaller () {
		Cover.SetTrigger ("Click");
		StartCoroutine (CallCommands());
	}

	IEnumerator CallCommands() {
		yield return new WaitForSeconds (1.25f);

		StartButton.SetBool ("IsHidden", false);

		yield return new WaitForSeconds (0.5f);
		
		SettingsButton.SetBool ("IsHidden", false);

		yield return new WaitForSeconds (0.5f);
		
		ExitButton.SetBool ("IsHidden", false);

	}

/// <summary>
/// If the Start Button is Clicked 
/// </summary>

	public void CharacterSelect () {

		StartCoroutine (NextMenu());

	}

	IEnumerator NextMenu() {


		Flash.SetTrigger ("IsPressed");
		StartFlash.SetTrigger ("IsPressed");

		Exit.GetComponent<Button> ().interactable = false;
		Setting.GetComponent<Button> ().interactable = false;

		yield return new WaitForSeconds (0.25f);

		StartInfo.SetBool ("IsHidden", true); // to prevent the Info Circle from not disappearing if the player doesn't move the mouse.

		PressEffect.SetTrigger ("IsPressed");

		yield return new WaitForSeconds (1.25f);
		
		StartButton.SetBool ("IsHidden", true);
		
		yield return new WaitForSeconds (0.5f);
		
		SettingsButton.SetBool ("IsHidden", true);
		
		yield return new WaitForSeconds (0.5f);
		
		ExitButton.SetBool ("IsHidden", true);

		yield return new WaitForSeconds (0.5f);

		MaskCanvas.SetTrigger ("Reappear");

		yield return new WaitForSeconds (1.25f);

		MainController.SwitchScene ("Stage1");

		//Application.LoadLevel (2);



		
	}

	public void ExitGame() {

		StartCoroutine (QuitApplication());
	}

	IEnumerator QuitApplication() {
		Flash3.SetTrigger ("IsPressed");
		ExitFlash.SetTrigger ("IsPressed");

		Starting.GetComponent<Button> ().interactable = false;
		Setting.GetComponent<Button> ().interactable = false;

		yield return new WaitForSeconds (0.25f);
		PressEffect3.SetTrigger ("IsPressed");

		yield return new WaitForSeconds (1.25f);
		Application.Quit ();
	}

}
