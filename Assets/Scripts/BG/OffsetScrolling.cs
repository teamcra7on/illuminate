﻿using UnityEngine;
using System.Collections;

public class OffsetScrolling : MonoBehaviour {
	
	public float ScrollSpeed;
	public Vector2 savedOffset;

	// Use this for initialization
	void Start () {
		savedOffset = GetComponent<Renderer>().sharedMaterial.GetTextureOffset ("_MainTex");
	}
	
	// Update is called once per frame
	void Update () {
		float y = Mathf.Repeat (Time.time * ScrollSpeed, 1);
		var offset = new Vector2 (0, y);
		GetComponent<Renderer>().sharedMaterial.SetTextureOffset ("_MainTex", offset);
	
	}

	void OnDisable () {
		GetComponent<Renderer>().sharedMaterial.SetTextureOffset ("_MainTex", savedOffset);
	}

}
