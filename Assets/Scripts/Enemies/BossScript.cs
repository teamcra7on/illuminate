﻿using UnityEngine;
using System.Collections;

public class BossScript : MonoBehaviour {

	public string pathToEnter;	// Enter the exact name of the path you made in iTweenPath
	public string[] pathsToFollow;
	public Animator BossSplash;
	public Animator BossLabelMask;
	public Animator BossLabel1;
	public Animator BossLabel2;
	public Animator BossLabel3;
	public Animator BossLabel4;

	public int speed = 3;

	// Use this for initialization
	void Start () {
		if (pathToEnter != null) {
			iTween.MoveTo (gameObject, iTween.Hash ("path", iTweenPath.GetPath (pathToEnter), "speed", speed, "onComplete", "ShootLaser"));	
		} else {
			Debug.LogError (transform.name + ": Path name not set");
			return;
		}

		StartCoroutine (BossIntroduction ());
	}

	//Step by step Boss Intro
	IEnumerator BossIntroduction () {

		GetComponent<Collider2D>().enabled = false;

		yield return StartCoroutine (CoroutineUtil.WaitforRealSeconds (1.2f));

		Time.timeScale = 0.0f;

		BossSplash.SetTrigger ("IsPlayed");

		yield return StartCoroutine (CoroutineUtil.WaitforRealSeconds (0.3f));
		
		BossLabelMask.SetTrigger ("IsPlayed");

		yield return StartCoroutine (CoroutineUtil.WaitforRealSeconds (0.2f));
		
		BossLabel1.SetTrigger ("IsPlayed");
		
		BossLabel2.SetTrigger ("IsPlayed");
		
		BossLabel3.SetTrigger ("IsPlayed");
		
		BossLabel4.SetTrigger ("IsPlayed");

		yield return StartCoroutine (CoroutineUtil.WaitforRealSeconds (3f));

		GetComponent<Collider2D>().enabled = true;
		Time.timeScale = 1f;
	}

	public void MovePath (int pathIndex, string doFunction) {
		iTween.MoveTo (gameObject, iTween.Hash ("path", iTweenPath.GetPath (pathsToFollow[pathIndex]), "speed", speed, "OnComplete", doFunction));
	}

	void MoveWhileShooting (int pathIndex, string doWhileMoving, string doComplete) {
		iTween.MoveTo (gameObject, iTween.Hash ("path", iTweenPath.GetPath (pathsToFollow[pathIndex]), "speed", speed, "OnUpdate", doWhileMoving, "OnComplete", doComplete));
	}

	void MoveNextPath () {
		MovePath (2, "backToStart");
	}

	void backToStart () {
		MovePath (3, "ShootLaser");
	}

	void Disable () {
		gameObject.SetActive (false);
	}

	void ShootLaser () {
		transform.GetChild(0).gameObject.SetActive (true);
	}

	void Shoot () {
		transform.GetChild(1).gameObject.SetActive (true);
	}
		
	void OnDisable () {
		SendMessageUpwards ("RemoveFromPool", transform);
	}
}