﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (AbandonParent))]
public class ShootingScript : MonoBehaviour {

	public GameObject player;
	Vector3 ProjectileDirection;

	bool allowLeave;
	Vector3 OriginalPosition;
	Vector3 RecordPosition;

	public enum ShootingType {
		followPlayer,
		shootDown
	};

	public ShootingType typeOfShot = ShootingType.followPlayer;

	void Awake () {
		if (player == null) {
			player = GameObject.FindGameObjectWithTag ("Player");
		}

		OriginalPosition = transform.position;
		RecordPosition = player.transform.position;

	}

	void Start () {

		ProjectileDirection = RecordPosition - transform.position;

		if (typeOfShot == ShootingType.followPlayer) {
			iTween.MoveTo (gameObject, iTween.Hash ("position", RecordPosition, "easetype", iTween.EaseType.linear, "speed", 4, "oncomplete", "Leave"));
		}
	}

	void Update () {
		if (allowLeave) {
			transform.Translate(ProjectileDirection.normalized * Time.deltaTime * 5);
		}

		if (typeOfShot == ShootingType.shootDown) {
			transform.Translate (Vector3.down * 5 * Time.deltaTime);
		}
	}

	void Leave () {
		allowLeave = true;
	}

	void OnBecameInvisible() {
		gameObject.SetActive (false);
	}

	void OnDisable () {
		transform.position = OriginalPosition;
		SendMessageUpwards ("RemoveFromPool", transform, SendMessageOptions.DontRequireReceiver);
	}
}
