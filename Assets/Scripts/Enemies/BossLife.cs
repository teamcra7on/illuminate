﻿using UnityEngine;
using System.Collections;

public class BossLife : MonoBehaviour {

	Collider2D col;
	public float depletionRate = 5f;

	[SerializeField]
	float bossLife = 100f;

	bool isDefeated;

	AbandonParent[] children;

	void Awake () {
		col = GetComponent<Collider2D>();
		children = GetComponentsInChildren<AbandonParent> ();
	}

	// Use this for initialization
	void HitByRay () {
		if (bossLife >= 0) {
			bossLife -= (depletionRate / 100);
		}
		GetComponent<SpriteRenderer> ().color = new Color(1f, 0.65f, 0.65f, 1f);
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		GetComponent<SpriteRenderer> ().color = new Color(1f, 1f, 1f, 1f);

		if (bossLife <= 0) {
			isDefeated = true;
			if (isDefeated) {
				StartCoroutine( DestroySelf() );
				isDefeated = false;
			}
		}
	}

	IEnumerator DestroySelf () {
		col.enabled = false;
		gameObject.GetComponent<Renderer>().enabled = false;
		if (!transform.GetChild (2).GetComponent<ParticleSystem>().isPlaying) {
			transform.GetChild (2).GetComponent<ParticleSystem>().Play (true);
		}

		if (transform.GetComponent<EnemyBasicMovement>() != null && transform.GetComponent<iTween>() != null) {
			transform.GetComponent<EnemyBasicMovement>().enabled = false;
			transform.GetComponent<iTween>().enabled = false;
		}


		foreach (AbandonParent child in children) {
			child.OnParentAbouttobeDisabled ();
		}

		yield return new WaitForSeconds (0.1f);

		gameObject.SetActive (false);

		yield return new WaitForSeconds (2f);

		// TODO: Figure out a foolproof way for this to work.
		if (transform.GetChild (0).GetComponent<ParticleSystem>().isStopped || transform.GetChild (0).GetComponent<ParticleSystem>().isPaused || !transform.GetChild (0).GetComponent<ParticleSystem>().IsAlive()) {
			gameObject.SetActive (false);	
		}

		yield return null;
	}
}
