﻿using UnityEngine;
using System.Collections;

public class RandomizePosition : MonoBehaviour {

	Vector3 position;

	// Use this for initialization
	void Start () {
		position = new Vector3 (Random.Range (-7.5f, 2.5f), 8.76f, 0.0f);
		transform.position = position;
	}
}
