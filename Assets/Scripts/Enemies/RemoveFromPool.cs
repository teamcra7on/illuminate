﻿using UnityEngine;
using System.Collections;

public class RemoveFromPool : MonoBehaviour {

	// Use this for initialization
	void Start () {
		// If parent doesn't have PatternChecker
		if (!transform.parent.gameObject.GetComponent<PatternChecker>()){

			// Throw an error
			Debug.LogError("Parent does not contain PatternChecker component", transform.parent);
		}
	}
	
	// Update is called once per frame
	void OnDisable () {
		SendMessageUpwards ("RemoveFromPool", this.transform, SendMessageOptions.DontRequireReceiver);
	}
}
