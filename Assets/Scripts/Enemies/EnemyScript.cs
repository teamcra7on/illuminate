﻿using UnityEngine;
using System.Collections;

public class EnemyScript : MonoBehaviour {

	public float delay = 0.5f;

	Collider2D col;
	bool hit;

	void Awake () {
		col = GetComponent<Collider2D>();
		col.enabled = false;
	}

	void OnEnable () {
		Invoke ("EnableHit", delay);
	}

	void EnableHit () {
		col.enabled = true;
	}

	IEnumerator DisableEnemy () {

		DetachChildren ();

		yield return new WaitForSeconds (0.1f);

		gameObject.SetActive (false);

		yield return new WaitForSeconds (2f);

		// TODO: Figure out a foolproof way for this to work.
		if (transform.GetChild (0).GetComponent<ParticleSystem>().isStopped || transform.GetChild (0).GetComponent<ParticleSystem>().isPaused || !transform.GetChild (0).GetComponent<ParticleSystem>().IsAlive()) {
			gameObject.SetActive (false);	
		}

	}

	void HitByRay () {
		// Logic for when enemy is hit

		if (!hit) {
			col.enabled = false;
			gameObject.GetComponent<Renderer>().enabled = false;
			if (!transform.GetChild (0).GetComponent<ParticleSystem>().isPlaying) {
				transform.GetChild (0).GetComponent<ParticleSystem>().Play (true);
			}

			if (transform.GetComponent<EnemyBasicMovement>() != null && transform.GetComponent<iTween>() != null) {
				transform.GetComponent<EnemyBasicMovement>().enabled = false;
				transform.GetComponent<iTween>().enabled = false;
			}

			StartCoroutine(DisableEnemy());

			hit = true;
		}
	}

	public void DetachChildren () {
		if (GetComponentInChildren<AbandonParent> () != null) {
			GetComponentInChildren<AbandonParent> ().OnParentAbouttobeDisabled ();
		}
	}

	void OnDisable () {
		SendMessageUpwards ("RemoveFromPool", transform);
	}
}
