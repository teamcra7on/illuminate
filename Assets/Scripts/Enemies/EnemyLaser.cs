﻿using UnityEngine;
using System.Collections;

public class EnemyLaser : MonoBehaviour {

	Transform tForm;
	public Transform player;
	LineRenderer line;

	public Material originalMaterial;
	public Material materialToChangeTo;

	public bool isReusable;

	Ray2D ray;
	RaycastHit2D hit;

	Vector2 inDirection;
	bool rayEnabled;

	public float delay = 1f;

	[SerializeField]
	bool showDebugLine;

	enum laserType {aimAtTarget, linear};
	[SerializeField]
	laserType typeOfLaser = laserType.aimAtTarget;
	
	void Awake () {
		tForm = gameObject.GetComponent<Transform> ();
		line = gameObject.GetComponent<LineRenderer> ();
		line.material = originalMaterial;
		line.enabled = false;
	}

	void OnEnable () {
		StartCoroutine (FireLaser ());
	}
		
	// Update is called once per frame
	void Update () {
		if (rayEnabled) {
			if (showDebugLine) {
				switch (typeOfLaser) {
				case laserType.aimAtTarget:
					Debug.DrawRay (tForm.position, inDirection.normalized * 100, Color.green);
					break;
				case laserType.linear:
					Debug.DrawRay (tForm.position, -tForm.transform.up * 50, Color.green);
					break;
				default:
					Debug.Log ("Laser not set");
					break;
				}
			}

			switch (typeOfLaser) {
			case laserType.aimAtTarget:
				ray = new Ray2D (tForm.position, inDirection.normalized);
				break;
			case laserType.linear:
				ray = new Ray2D (tForm.position, -tForm.transform.up * 50);
				break;
			default:
				Debug.Log ("Laser not set");
				break;
			}

			hit = Physics2D.Raycast (ray.origin, ray.direction, 10);
			if (hit.collider != null) {
				if(hit.transform.CompareTag("Player")){
					player.GetComponent<Lives> ().LoseLive ();
				}
					
			}
		}
	}

	IEnumerator FireLaser () {
		if (typeOfLaser == laserType.aimAtTarget) {
			inDirection = player.position - tForm.position;
		}

		line.enabled = true;
		line.SetPosition (0, tForm.position);

		switch (typeOfLaser) {
		case laserType.aimAtTarget:
			line.SetPosition (1, inDirection.normalized * 1500);
			break;
		case laserType.linear:
			line.SetPosition (1, tForm.position - tForm.transform.up * 50);
			break;
		default:
			Debug.Log ("Laser not set");
			break;
		}

		yield return new WaitForSeconds (delay);

		rayEnabled = true;
		line.material = materialToChangeTo;

		yield return new WaitForSeconds (delay);
	
		rayEnabled = false;
		line.enabled = false;
		line.material = originalMaterial;
		SendMessageUpwards ("RemoveFromPool", transform, SendMessageOptions.DontRequireReceiver);

		gameObject.SetActive (false);
		yield return null;
	}
}
