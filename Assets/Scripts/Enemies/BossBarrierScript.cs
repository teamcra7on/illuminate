﻿using UnityEngine;
using System.Collections;

public class BossBarrierScript : MonoBehaviour {

	public Animator BossBarrier;
	bool hit;

	// Use this for initialization
	void Start () {
		hit = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (hit) {
			BossBarrier.SetBool ("Collided", true);
			DialogueWriter.FourthTrigger = true;
		} else {
			BossBarrier.SetBool ("Collided", false);
		}
	}
	
	void HitByRay () {
		StopCoroutine ("BarrierOn");
		StartCoroutine ("BarrierOn");
	}

	IEnumerator BarrierOn() {
		hit = true;
		yield return new WaitForSeconds (0.8f);
		hit = false;
	}

}
