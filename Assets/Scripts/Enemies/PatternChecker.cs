﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PatternChecker : MonoBehaviour {

	public StageSpawner spawner;

	public enum checkTypes {
		Enemies,
		Bullets,
		DisableOnly
	};

	[SerializeField]
	private checkTypes typeOfChildren = checkTypes.Enemies;

	public List <Transform> children = new List<Transform>();

	void Awake () {
		if (spawner == null && typeOfChildren == checkTypes.Enemies) {
			spawner = GameObject.FindWithTag ("Spawner").GetComponent<StageSpawner>();
		}
	}

	// Use this for initialization
	void OnEnable () {
		foreach (Transform child in transform) {
			children.Add (child);
		}
	}

	void Update () {
		if (children.Count == 0) {
			this.gameObject.SetActive (false);
		}
	}

	void RemoveFromPool (Transform transform) {
		children.Remove (transform);
	}

	void OnDisable () {
		if (typeOfChildren == checkTypes.Enemies) {
			spawner.NextPattern ();
		} else if (typeOfChildren == checkTypes.Bullets) {
			SendMessageUpwards ("AllowEnemyToLeave");
		} else {
			return;
		}
	}
}
