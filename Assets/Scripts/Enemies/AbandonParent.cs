﻿using UnityEngine;
using System.Collections;

public class AbandonParent : MonoBehaviour {

	public void OnParentAbouttobeDisabled () {
		transform.parent = null;
	}
}
