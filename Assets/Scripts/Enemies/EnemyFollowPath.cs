﻿using UnityEngine;
using System.Collections;

public class EnemyFollowPath : MonoBehaviour {

	public string pathToFollow;	// Enter the exact name of the path you made in iTweenPath
	public string pathToLeave;

	public bool rotateEnemyAlongPath = true;

	public enum TypeOfEnemy {
		FollowPathOnly,
		StopShootAndLeave,
		EnterOnly
	};

	[SerializeField]
	TypeOfEnemy enemyType;

	// Use this for initialization
	void Start () {
		if (pathToFollow != null) {
			if (enemyType == TypeOfEnemy.FollowPathOnly) {
				iTween.MoveTo (gameObject, iTween.Hash ("path", iTweenPath.GetPath (pathToFollow), "orienttopath", rotateEnemyAlongPath, "looktime", 0.2f, "time", 5, "oncomplete", "Disable"));
			} else if (enemyType == TypeOfEnemy.StopShootAndLeave) {
				iTween.MoveTo (gameObject, iTween.Hash ("path", iTweenPath.GetPath (pathToFollow), "time", 5, "oncomplete", "Shoot"));
			} else if (enemyType == TypeOfEnemy.EnterOnly) {
				iTween.MoveTo (gameObject, iTween.Hash ("path", iTweenPath.GetPath (pathToFollow), "time", 5));
			}
		} else {
			Debug.LogError (transform.name + ": Path name not set");
			return;
		}
	}

	void Disable () {
		gameObject.SetActive (false);
	}

	void Shoot () {
		transform.GetChild(1).gameObject.SetActive (true);
	}

	void AllowEnemyToLeave () {
		if (pathToLeave != "") {
			iTween.MoveTo (gameObject, iTween.Hash ("path", iTweenPath.GetPath (pathToLeave), "time", 5, "oncomplete", "Disable"));
		}
	}

	void OnDisable () {
		SendMessageUpwards ("RemoveFromPool", transform);
	}
}