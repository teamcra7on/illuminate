﻿using UnityEngine;
using System.Collections;

public class BossCaller : MonoBehaviour {

	public int PathIndex;
	public string bossFunctionToCall;

	BossScript boss;

	// Use this for initialization
	void Start () {
		boss = GetComponentInParent<BossScript>();
	}
	
	// Update is called once per frame
	void OnDisable () {
		boss.MovePath (PathIndex, bossFunctionToCall);
	}
}
