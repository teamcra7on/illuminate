﻿using UnityEngine;
using System.Collections;

public class EnemyBasicMovement : MonoBehaviour {

	public GameObject player;
	public bool followPlayer;

	bool allowLeave;

	void Start () {
		foreach (Transform child in transform) {
			child.gameObject.SetActive (true);
		}

		if (followPlayer) {
			if (player == null) {
				player = GameObject.FindGameObjectWithTag ("Player");
			}

			iTween.MoveTo (gameObject, iTween.Hash ("position", player.transform.position, "easetype", iTween.EaseType.linear, "speed", 4, "oncomplete", "Leave"));
		}
	}

	// Update is called once per frame
	void Update () {
		transform.Translate (Vector3.down * 5 * Time.deltaTime);

		if (allowLeave) {
			transform.Translate (Vector3.down * 5 * Time.deltaTime);
		}
	}

	void Leave () {
		allowLeave = true;
	}
}
