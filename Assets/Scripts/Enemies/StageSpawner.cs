﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Patterns {
	public GameObject[] pattern;
	public float startDelay = 0.1f;		// Delay before the pattern starts
}

public class StageSpawner : MonoBehaviour {

	public Animator Darkness; // For the Stage End Animation
	public Camera cam;
	public Patterns[] enemySettings;

	public bool endStageNow;

	bool isSpawning;

	int i;

	// Use this for initialization
	void Start () {
		if (cam == null) {
			cam = Camera.main;
		}
	}

	void Update () {
		if (!isSpawning && i < enemySettings.Length) {
			isSpawning = true;
			StartSpawn ();		
		} else if (i >= enemySettings.Length || endStageNow) {
			EndStage ();
		}
	}

	void StartSpawn () {
		StopCoroutine (Spawn ());
		StartCoroutine (Spawn ());
	}

	IEnumerator Spawn () {
		yield return new WaitForSeconds(enemySettings[i].startDelay);

		for (int k = 0; k < enemySettings[i].pattern.Length; k++) {
			enemySettings[i].pattern[k].gameObject.SetActive (true);
		}
		
		yield return null;
	}

	public void NextPattern () {
		i++;
		isSpawning = false;
	}

	void EndStage () {
		// Logic for stage end goes here.
		DialogueWriter.FifthTrigger = true;
		Darkness.SetTrigger("IsTriggered");
		Invoke ("LoadMenu", 5);
		// EndOfStage; // Call for a global end stage script TODO: Actually make it.
	}

	void LoadMenu () {
		Time.timeScale = 1.0f;
		Application.LoadLevel (0);
	}
}
