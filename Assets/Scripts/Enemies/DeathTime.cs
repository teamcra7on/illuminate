﻿using UnityEngine;
using System.Collections;

public class DeathTime : MonoBehaviour {

	float DeathTimer; //TEMP FIX ATTEMPT... REFERENCE TO THE VALUE OF THE TIMER FOR THE PROJECTILES
	public float DeathTimerValue = 10f; // The value the gets set on start

	// Use this for initialization
	void OnEnable () {
		DeathTimer = DeathTimerValue; //TEMP FIX ATTEMPT... 5 SECONDS LIFESPAN 	
	}
	
	// Update is called once per frame
	void Update () {
		DeathTimer -= (1f * Time.deltaTime); // TEMP FIX ATTEMPT... Death is coming	

		if (DeathTimer <= 0) {
			gameObject.SetActive(false); // TEMP FIX ATTEMPT... Disable after Lifespan
		}
	}
}
