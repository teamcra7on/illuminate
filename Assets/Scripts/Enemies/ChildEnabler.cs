﻿using UnityEngine;
using System.Collections;

public class ChildEnabler : MonoBehaviour {

	public float delay = 0.5f;
	public bool isDelayed;
	bool isEnabled;

	// Use this for initialization
	void OnEnable () {
		isEnabled = true;
		if (isDelayed && isEnabled) {
			StopCoroutine (DelaySpawner ());
			StartCoroutine (DelaySpawner ());
		}
		else if (!isDelayed && isEnabled) {
			Spawner ();
		}
	}

	void Spawner () {
		foreach (Transform child in transform) {
			child.gameObject.SetActive (true);
		}
		isEnabled = false;
	}

	IEnumerator DelaySpawner () {

		yield return new WaitForSeconds(1f);

		foreach (Transform child in transform) {
			child.gameObject.SetActive (true);
			yield return new WaitForSeconds (delay);
		}

		isEnabled = false;
		yield return null;
	}
}