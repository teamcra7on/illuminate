﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent (typeof (LineRenderer))]
public class LaserScript : MonoBehaviour {

	public LayerMask CollidedLayerMasks;
	public GameObject BatteryObject;
	public Animator Player;
	Image Battery;

	Transform tForm;
	LineRenderer line;

	Ray2D ray;
	RaycastHit2D hit;

	Vector3 inDirection;

	public int nReflections = 5;

	int nPoints;

	public float BatteryCapacity = 5f;

	bool FireisAllowed = true;
	public bool Charging;
	public bool IsFiring;

	// Use this for initialization
	void Awake () {
		tForm = gameObject.GetComponent<Transform> ();
		line = gameObject.GetComponent<LineRenderer> ();
		line.enabled = false;
		Battery = BatteryObject.GetComponent<Image> ();
		IsFiring = false;
	}

	void Charge () {
		BatteryCapacity = BatteryCapacity + 1.3f *Time.deltaTime;	
	}
	
	// Update is called once per frame
	void Update () {

		Battery.fillAmount = BatteryCapacity / 5f;
		Battery.color = new Color (1f, Battery.fillAmount, Battery.fillAmount);
		StartCoroutine(WaitbeforeCharging());

		if (!IsFiring && BatteryCapacity < 5f) {
			StartCoroutine(WaitbeforeCharging());
		}

		else {
			Charging = false;
		}

				if (Charging) {
					Charge ();
				}

				if (BatteryCapacity <= 0) {
						FireisAllowed = false;
						Charging = true;
						
				}

				if (BatteryCapacity >= 5f) {
					BatteryCapacity = 5f;
					FireisAllowed = true;
					Charging = false;
				}

				if (Input.GetButtonUp ("Fire1")) {

							IsFiring = false;

						}

				if (Input.GetButtonDown ("Fire1") && Time.timeScale >= 1f) {
						if (BatteryCapacity > 0 && FireisAllowed) {
								StopCoroutine ("FireLaser");
								StartCoroutine ("FireLaser");
						}

				}

				else {
					
					Player.SetBool("IsFiring", false);
					
					
				}
	}

	IEnumerator WaitbeforeCharging() {

		yield return new WaitForSeconds (1.5f);

		Charging = true;


		yield return null;

	}

	IEnumerator FireLaser () {
		line.enabled = true;

		while (Input.GetButton ("Fire1") && FireisAllowed) {

			Player.SetBool("IsFiring", true);
			IsFiring = true;

			BatteryCapacity = BatteryCapacity - 1 * Time.deltaTime;

			line.GetComponent<Renderer>().material.mainTextureOffset = new Vector2 (0, Time.time);

			nReflections = Mathf.Clamp (nReflections, 1, nReflections);
			ray = new Ray2D (tForm.position, tForm.up);

			Debug.DrawRay(tForm.position, tForm.up * 100, Color.red);

			nPoints = 2;
			line.SetVertexCount (nPoints);
			line.SetPosition (0, tForm.position);


			for (int i = 0; i <= nReflections; i++) {
				if (i == 0) {
					hit = Physics2D.Raycast (ray.origin, ray.direction, 100, CollidedLayerMasks);

					if (hit.collider != null) {

						if (hit.transform.CompareTag ("Mirror")) {
						inDirection = Vector3.Reflect (ray.direction, hit.normal);
						ray = new Ray2D (hit.point, inDirection);

						Debug.DrawRay (hit.point, hit.normal * 3, Color.blue);
						Debug.DrawRay (hit.point, inDirection * 100, Color.magenta);

						}

						//Debug.Log ("Object name: " + hit.transform.name);

						if (hit.transform.CompareTag ("Enemy")) {
							hit.transform.SendMessage ("HitByRay", SendMessageOptions.DontRequireReceiver);
						}

						if (nReflections == 1) {
							line.SetVertexCount (++nPoints);
						}

						line.SetPosition (i + 1, hit.point);
					
					} else {
						line.SetPosition (i + 1, ray.GetPoint(100));
					}
				} 


				else {
					hit = Physics2D.Raycast (ray.origin, ray.direction, 100, CollidedLayerMasks);

					if (hit.collider != null) {

						if (hit.transform.CompareTag ("Mirror")) {

						inDirection = Vector3.Reflect (inDirection, hit.normal);
						ray = new Ray2D (hit.point, inDirection);

						Debug.DrawRay (hit.point, hit.normal * 3, Color.blue);
						Debug.DrawRay (hit.point, inDirection * 100, Color.magenta);
						
						}
						//Debug.Log ("Object name: " + hit.transform.name);

						if (hit.transform.CompareTag ("Enemy")) {
							hit.transform.SendMessage ("HitByRay", SendMessageOptions.DontRequireReceiver);
						}

						line.SetVertexCount (++nPoints);
						line.SetPosition (i + 1, hit.point);
					} else {
						line.SetVertexCount (++nPoints);
						line.SetPosition (i + 1, ray.GetPoint(100));
					}
				}
			}

			yield return null;
		}

		line.SetVertexCount (0);
		line.enabled = false;
	}
}