using UnityEngine;
using System.Collections;

public class MirrorScript : MonoBehaviour {

	public float shootMirrorForce = 10f;	// How fast the mirror moves when shot
	BoxCollider2D mirrorCollider;	// Reference to this object's collider
	public float MirrorActivate; // Reference to the time before the Mirror's Collider Activates
	public float MirrorDuration; // Reference to the Mirror's Duration

	// Use this for initialization
	void Start () {
		mirrorCollider = GetComponent<BoxCollider2D> ();
		MirrorDuration = 10f;
	}

	void FixedUpdate () {

		if (MirrorActivate > 0) {
			MirrorActivate = MirrorActivate - 1 * Time.deltaTime;
		}

		mirrorCollider.enabled |= MirrorActivate <= 0;

		if(MirrorDuration <= 0) {
			GetComponent<Animator>().SetTrigger("Death");
			mirrorCollider.enabled = false;
			Destroy (gameObject, 0.3f);
		}

		MirrorDuration = MirrorDuration - 1 * Time.deltaTime;

	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.gameObject.tag == ("Enemy")) {
			GetComponent<Animator> ().SetTrigger ("Death");
			mirrorCollider.enabled = false;
			Destroy (gameObject, 0.3f);
		}
	}

	void OnMouseOver() {
		if (Input.GetButtonDown ("Fire3")) {
			GetComponent<Animator>().SetTrigger("Death");
			mirrorCollider.enabled = false;
			Destroy (gameObject, 0.3f);
		}
	}
	

	void OnBecameInvisible () {
		GetComponent<Animator>().SetTrigger("Death");
		mirrorCollider.enabled = false;
		Destroy (gameObject, 0.3f);
	}
}
