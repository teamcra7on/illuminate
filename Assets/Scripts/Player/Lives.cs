﻿using UnityEngine;
using System.Collections;

public class Lives : MonoBehaviour {

	public int liveCount = 3;
	public Animator Hit;

	void Activate () {
		GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
		GetComponent<Collider2D>().enabled = true;
	}

	void OnTriggerEnter2D (Collider2D collider) {
		if (collider.CompareTag ("Enemy")) {
			LoseLive ();
		}
	}

	// Call this to lose a live
	public void LoseLive () {
		Hit.SetTrigger( "IsHit" );
		liveCount--;
		GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.5f);
		GetComponent<Collider2D>().enabled = false;
		Invoke ("Activate", 2);	
	}
}
