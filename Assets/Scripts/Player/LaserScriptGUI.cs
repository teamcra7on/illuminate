﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (LaserScript))]
public class LaserScriptGUI : MonoBehaviour {

	public GameObject player;
	LaserScript laser;

	public Animator ChargeNotice;
	public Animator ChargeOk;

	// Use this for initialization
	void Start () {
		laser = GetComponent<LaserScript> ();
		player = GameObject.FindGameObjectWithTag ("Player");
	}

	void ChargeGUI () {
		//player.GetComponent<LiveGUI>().Alert.SetTrigger ( "Exhaust" );
		//player.GetComponent<LiveGUI>().Alert2.SetTrigger ( "Exhaust" );

		player.GetComponent<Animator>().SetBool("IsCharging", true);
		if (laser.BatteryCapacity < 3.5f && laser.Charging) {	
			ChargeNotice.SetBool ("IsHidden", false);
		}

		else if (laser.BatteryCapacity >= 3.5f && laser.BatteryCapacity <= 4f && laser.Charging) {
			ChargeOk.SetTrigger("IsReady");
			ChargeNotice.SetBool ("IsHidden", true);
		}

	}

	void Update () {
		if (laser.Charging) {
			ChargeGUI ();
		}

		else {
			player.GetComponent<Animator>().SetBool("IsCharging", false);
		}

		if (laser.BatteryCapacity >= 5f) {
			player.GetComponent<LiveGUI>().Alert.SetTrigger ( "Stable" );
			player.GetComponent<LiveGUI>().Alert2.SetTrigger ( "Stable" );


		}
	}
}
