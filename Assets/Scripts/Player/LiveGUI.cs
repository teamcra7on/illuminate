﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (Lives))]
public class LiveGUI : MonoBehaviour {

	public Lives liveScript;

	public Animator HP1;
	public Animator HP2;
	public Animator HP3;
	public Animator Alert;
	public Animator Alert2;

	public AudioSource Alarm;

	// Use this for initialization
	void Start () {
		liveScript = GetComponent<Lives> ();
		Alarm.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (liveScript.liveCount <= 2) {
			HP1.SetTrigger( "IsHit" );
			Alert.SetTrigger ( "Yellow" );
			Alert2.SetTrigger ( "Yellow" );

		}

		if (liveScript.liveCount <= 1) {
			HP2.SetTrigger( "IsHit" );
			Alert.SetTrigger ( "Red" );
			Alert2.SetTrigger ( "Red" );

			Play();
		}

		if (liveScript.liveCount <= 0) {
			HP3.SetTrigger( "IsHit" );
			Application.LoadLevel (0);
		}
	}

	void Play () {
		Alarm.enabled = true;
		Alarm.mute = false;
	}
}
