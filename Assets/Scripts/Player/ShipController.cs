﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Boundary {
	public float xMin, xMax, yMin, yMax;
}

public class ShipController : MonoBehaviour {

	public float moveSpeed = 1f;	// The speed at which the player moves
	public float newMirrorRate = 1f; // Time till a new mirror can be made
	public float nextMirror = 1f;	// A new mirror will be made when this is met
	public GameObject mirror;	// Reference to the ship's mirror
	public GameObject newMirror;	// Reference to the mirror prefab
	public Boundary boundary;	// Reference to this script's Boundary class
	public int MirrorCount = 5; // This sets the maximum number of Mirrors
	public GameObject[] MirrorCounter; // Reference to all Mirrors
	public Animator PlayerMovement; // Reference to the Player Sprite Animator
	public Animator MirrorRotator; // Reference to the Rotating Mirror Sprite Animator
	public Vector2 CursorClick; // Position of the Mirror

	// Update is called once per frame
	void FixedUpdate () {
		Move ();
		RotateMirror ();

		MirrorCounter = GameObject.FindGameObjectsWithTag ("Mirror");

		if(MirrorCounter.Length > MirrorCount)
		{
			Destroy(MirrorCounter[0].gameObject,0.3f);
			DialogueWriter.SecondTrigger = true;
			MirrorCounter[0].GetComponent<Animator>().SetTrigger("Death");
		}

		if (Input.GetButton ("Fire2") && Time.time > nextMirror) {

			ShootMirror ();
		}
	}

	void Move () {
		// cache the player inputs
		float moveH = Input.GetAxis ("Horizontal");
		float moveV = Input.GetAxis ("Vertical");

		// Move the player
		var movement = new Vector3 (moveH, moveV, 0.0f);
		GetComponent<Rigidbody2D>().velocity = movement * moveSpeed;

		GetComponent<Rigidbody2D>().position = new Vector3  (
			Mathf.Clamp (GetComponent<Rigidbody2D>().position.x, boundary.xMin, boundary.xMax),  
			Mathf.Clamp (GetComponent<Rigidbody2D>().position.y, boundary.yMin, boundary.yMax),
			0.0f
		);

			PlayerMovement.SetFloat("Direction", moveH);
	}

	IEnumerator FadeOut() {
			MirrorRotator.SetBool ("IsHidden", false);
			yield return new WaitForSeconds (5f);	
			MirrorRotator.SetBool ("IsHidden", true);
	}

	void RotateMirror () {

		//Mirror Rotation Guide fades out if Mirror is not rotated (still buggy though :P)
		if (Input.GetAxisRaw ("Mouse ScrollWheel")!= 0){
			StartCoroutine ( FadeOut () );
		} else {
			StopCoroutine ( FadeOut () );
		}
			
		if (Input.GetAxis("Mouse ScrollWheel") > 0) {
			mirror.transform.Rotate(Vector3.back * 12f, Space.Self);
		}
		else if (Input.GetAxis("Mouse ScrollWheel") < 0) {
			mirror.transform.Rotate(Vector3.forward * 12f, Space.Self);
		}

	}

	void ShootMirror () {
		CursorClick = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		if(CursorClick.x > -8f && CursorClick.x < 3f && CursorClick.y > -2f && CursorClick.y < 9f) {
			nextMirror = Time.time + newMirrorRate;
			Instantiate (newMirror, CursorClick , mirror.transform.rotation);
			DialogueWriter.ThirdTrigger = true;
		}
	}
}
