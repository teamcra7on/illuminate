﻿using UnityEngine;
using System.Collections;

public class GlowHover : MonoBehaviour {
	
	public Animator OuterRing;
	public Animator InnerRing;
	public Animator Sun;
	public Animator Title;

	void OnMouseEnter() {

		OuterRing.SetBool ("IsHidden", false);
		InnerRing.SetBool ("IsHidden", false);
		Sun.SetBool ("IsHidden", false);
		Title.SetBool ("IsHidden", false);

	}

	void OnMouseExit() {
		
		OuterRing.SetBool ("IsHidden", true);
		InnerRing.SetBool ("IsHidden", true);
		Sun.SetBool ("IsHidden", true);
		Title.SetBool ("IsHidden", true);
	
	}

}
