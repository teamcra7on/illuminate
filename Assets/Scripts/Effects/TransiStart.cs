﻿using UnityEngine;
using System.Collections;

public class TransiStart : MonoBehaviour {

	public Animator Transistart;
	
	// Update is called once per frame
	void Update () {
		StartCoroutine (SwipeIn ());
	}

	IEnumerator SwipeIn () {
		yield return new WaitForSeconds(2.0f);
		Transistart.SetBool ("Move", true);
	}
}
